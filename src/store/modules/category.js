const state = {
  category1Id: null,
  category2Id: null,
  category3Id: null
};
const mutations = {
  setCategory1Id(state, payload) {
    state.category1Id = payload.category1Id;
  },
  setCategory2Id(state, payload) {
    console.log(payload);
    state.category2Id = payload.category2Id;
  },
  setCategory3Id(state, payload) {
    state.category3Id = payload.category3Id;
  }
};
const actions = {};
const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
