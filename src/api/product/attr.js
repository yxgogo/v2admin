import request from "@/utils/request";

export const getAttrListApi = (category1Id, category2Id, category3Id) => {
  return request.get(
    `/admin/product/attrInfoList/${category1Id}/${category2Id}/${category3Id}`
  );
};
