import request from "@/utils/request";

export const getCategory1ListApi = () => {
  return request.get(`/admin/product/getCategory1`);
};

/**
 * 获取二级分类
 * @param category1Id 一级分类id
 * @returns CategoryListModel
 */
export const getCategory2ListApi = category1Id => {
  return request.get(`/admin/product/getCategory2/${category1Id}`);
};

/**
 * 获取三级分类
 * @param category2Id 二级分类id
 * @returns CategoryListModel
 */
export const getCategory3ListApi = category2Id => {
  return request.get(`/admin/product/getCategory3/${category2Id}`);
};
