import request from "@/utils/request";

/* 获取分页的列表 */
export const getPageListApi = (page, limit) => {
  return request.get(`/admin/product/baseTrademark/${page}/${limit}`);
};

/* 添加 */

export const saveTrademarkApi = (tmName, logoUrl) => {
  return request.post(`/admin/product/baseTrademark/save`, {
    tmName,
    logoUrl
  });
};

/* 修改 */

export const updateTrademarkApi = tm => {
  return request.put(`/admin/product/baseTrademark/update`, tm);
};
