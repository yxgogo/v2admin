import request from "@/utils/request";

/**
 * 获取权限列表
 * @returns PermissionListResponseModel
 */
export const getPermissionListApi = () => {
  return request.get(`/admin/acl/permission`);
};
