import request from "@/utils/request";

/* 获取角色列表 */

export const getRoleListApi = (page, limit, roleName) => {
  console.log(2);
  return request.get(`/admin/acl/role/${page}/${limit}`, {
    params: {
      roleName
    }
  });
};

/* 更新角色 */

export const updateRoleApi = role => {
  return request.put(`/admin/acl/role/update`, role);
};

/**
 * 保存一个新角色
 * @param role 角色对象
 * @returns null
 */
export const saveRoleApi = role => {
  return request.post(`/admin/acl/role/save`, role);
};

/* 


 ANTH

*/

/* 请求一个角色的全部权限 */
export const getAssignRoleApi = roleId => {
  return request.get(`/admin/acl/permission/toAssign/${roleId}`);
};

export const assignRoleApi = (roleId, permissionId) => {
  return request.post(`/admin/acl/permission/doAssign`, null, {
    params: {
      roleId,
      permissionId
    }
  });
};
