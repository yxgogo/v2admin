import request from "@/utils/request";

/* 请求用户分页列表数据的请求 */

export const getUserListApi = (page, limit, searchParams) => {
  return request.get(`/admin/acl/user/${page}/${limit}`, {
    params: searchParams
  });
};

/* 添加user */
export const addUserApi = user => {
  return request.post(`/admin/acl/user/save`, user);
};

/* 更改user */

export const updateUserApi = user => {
  return request.put(`/admin/acl/user/update`, user);
};

/* 删除user */
export const removeUserApi = id => {
  return request.delete(`/admin/acl/user/remove/${id}`);
};

/* 删除多个 */
export const removeManyApi = ids => {
  return request.delete(`/admin/acl/user/batchRemove`, {
    data: ids
  });
};

/* 获得角色列表 */

export const getUserRoleListApi = userId => {
  return request.get(`/admin/acl/user/toAssign/${userId}`);
};

/* 分配角色 */

export const assignUserRoleListApi = (userId, roleId) => {
  return request.post(`/admin/acl/user/doAssign`, null, {
    params: {
      userId,
      roleId
    }
  });
};
