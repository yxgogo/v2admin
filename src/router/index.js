import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

/* Layout */
import Layout from "@/layout";
export const constantRoutes = [
  {
    path: "/login",
    component: () => import("@/views/login/index"),
    hidden: true
  },

  {
    path: "/404",
    component: () => import("@/views/404"),
    hidden: true
  },

  {
    path: "/",
    component: Layout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        component: () => import("@/views/Home/index"),
        meta: { title: "首页", icon: "el-icon-s-home" }
      }
    ]
  },

  // {
  //   /*  /dashboard */
  //   path: "/",
  //   component: Layout,
  //   redirect: "/dashboard",
  //   children: [
  //     {
  //       path: "dashboard",
  //       name: "Home",
  //       component: () => import("@/views/Home/index")
  //     }
  //   ],
  //   hidden: true
  // },
  {
    path: "/acl",
    name: "Acl",
    component: Layout,
    redirect: "/acl/user/list",
    meta: {
      title: "权限管理",
      icon: "el-icon-s-tools"
    },
    children: [
      {
        name: "User",
        path: "acl/user/list",
        component: () => import("@/views/acl/user"),
        meta: {
          title: "用户管理"
        }
      },
      {
        name: "Role",
        path: "acl/role/list",
        component: () => import("@/views/acl/role"),
        meta: {
          title: "角色管理"
        }
      },
      {
        name: "RoleAuth",
        path: "acl/role/auth",
        component: () => import("@/views/acl/RoleAuth"),
        meta: {
          title: "角色管理"
        },
        hidden: true
      },

      {
        name: "Permission",
        path: "/acl/permission/list",
        component: () => import("@/views/acl/permission/index.vue"),
        meta: {
          title: "菜单管理"
        }
      }
    ]
  },
  /* 商品管理 start */
  {
    path: "/product",
    name: "Product",
    component: Layout,
    redirect: "/product/category/list",
    meta: {
      title: "商品管理",
      icon: "el-icon-s-goods"
    },
    children: [
      {
        name: "Category",
        path: "category/list",
        component: () => import("@/views/product/category"),
        meta: {
          title: "分类管理"
        }
      },
      {
        name: "Trademark",
        path: "trademark/list",
        component: () => import("@/views/product/trademark"),
        meta: {
          title: "品牌管理"
        }
      },
      {
        name: "Attr",
        path: "/product/attr/list",
        component: () => import("@/views/product/attr"),
        meta: {
          title: "平台属性管理"
        }
      },
      {
        name: "Spu",
        path: "/product/spu/list",
        component: () => import("@/views/product/spu"),
        meta: {
          title: "SPU管理"
        }
      },
      {
        name: "Sku",
        path: "/product/sku/list",
        component: () => import("@/views/product/sku"),
        meta: {
          title: "SKU管理"
        }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: "*", redirect: "/404", hidden: true }
];

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  });

const router = createRouter();

/* /* router.options.routes[3].children */
/* router.options.routes[3].children.push({
  name: "RoleAuth",
  path: "acl/role/auth",
  component: () => import("@/views/acl/RoleAuth"),
  meta: {
    title: "角色管理"
  }
});
 */

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
